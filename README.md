NuPack
------

Code example for Nulogy.

Requirements
------------

 * NodeJS: v0.10 (or newer)
 * NPM: v1.4.0 (or newer)
 * GNU Make (or equivalent)

Installation
------------

Simply run `make` from this directory!

Usage
-----

This is simply a library with test cases as example code. See `test/nupack.spec.js` for usage examples.



