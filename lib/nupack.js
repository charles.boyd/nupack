/**
 * NuPack
 */

var _ = require('lodash');

var Job = require('./models/Job.js');
var Asset = require('./models/Asset.js');
var MarkupTable = require('./models/MarkupTable.js');

function NuPack() {
  this.job = new Job();
  this.markupTable = new MarkupTable();
}

NuPack.prototype.getCurrentJob = function() {
  return this.job;
};

NuPack.prototype.getMarkupTable = function() {
  return this.markupTable;
};

NuPack.prototype.addAsset = function(value, type) {
  var asset = new Asset(value, type);
  this.job.appendAsset(asset);
};

NuPack.prototype.addWorkers = function(extraWorkers) {
  var numWorkers = this.job.getWorkerCount() + extraWorkers;
  this.job.setWorkerCount(numWorkers);
};

NuPack.prototype.computeJobQuote = function() {
  var currentJob = this.getCurrentJob();
  var markupTable = this.getMarkupTable();

  var jobQuote = 0.0;

  _.forEach(this.job.listAssets(), function(asset) {
    var assetBaseQuote = asset.getValue() + ( asset.getValue() * markupTable.getBaseMarkupRate() );
    var assetWorkPrice = assetBaseQuote * ( currentJob.getWorkerCount() * markupTable.getWorkerMarkupRate() );
    var assetMarkupPrice = assetBaseQuote *  markupTable.getMarkupRateForType( asset.getType() );
    var assetQuote = assetBaseQuote + assetWorkPrice + assetMarkupPrice;
    jobQuote += _.round( assetQuote, 2 );
  });

  return jobQuote;
};

NuPack.prototype.resetJob = function() {
  this.job = new Job();
};

module.exports = new NuPack();
