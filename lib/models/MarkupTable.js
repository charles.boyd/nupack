var _ = require('lodash');

/**
  * Create a new MarkupTable instance.
  *
  * Options
  * @class Represents a MarkupTable singleton
 */
function MarkupTable() {
  this.baseRate = 0.0;
  this.workerRate = 0.0;
  this.markupTable = {};
}

/**
  * public functions
 */

MarkupTable.prototype.getBaseMarkupRate = function() {
  return this.baseRate;
};

MarkupTable.prototype.setBaseMarkupRate = function(baseRate) {
  this.baseRate = baseRate;
};

MarkupTable.prototype.getWorkerMarkupRate = function() {
  return this.workerRate;
};

MarkupTable.prototype.setWorkerMarkupRate = function(workerRate) {
  return this.workerRate = workerRate;
};

MarkupTable.prototype.getMarkupRateForType = function(assetType) {
  if ( _.has(this.markupTable, assetType) ) {
    return this.markupTable[assetType];
  }

  return 0.0;
};

MarkupTable.prototype.setMarkupRateForType = function(assetType, rate) {
  this.markupTable[assetType] = rate;
};

module.exports = MarkupTable;
