var _ = require('lodash');

/**
  * Create a new Job instance.
  *
  * Options
  * @class Represents a Job
 */
function Job() { //Constructor
  this.assetList = [];
  this.workerCount = 1;
}

/**
  * public functions
 */

Job.prototype.appendAsset = function(asset) {
  this.assetList.push(asset);
};

Job.prototype.listAssets = function() {
  return this.assetList;
};

Job.prototype.getWorkerCount = function() {
  return this.workerCount;
};

Job.prototype.setWorkerCount = function(workerCount) {
  if ( workerCount >= 1 ) {
    this.workerCount = _.round( workerCount, 0 );
  }
};

Job.prototype.netAssetValue = function() {

  var netValue = 0;

  _.forEach(this.assetList, function(asset) {
    netValue += asset.getValue();
  });

  return netValue;
};

module.exports = Job;
