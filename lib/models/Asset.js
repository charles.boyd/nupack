/**
  * Create a new Asset instance.
  *
  * Options
  * @class Represents a Asset
  * @param assetValue represents the value of the given asset
  * @param assetType product category corresonding to this asset
 */
function Asset(assetValue, assetType) { //Constructor
  this.assetValue = assetValue;
  this.assetType = assetType;
}

/**
  * public functions
 */

Asset.prototype.getValue = function() {
  return this.assetValue;
};

Asset.prototype.getType = function() {
  return this.assetType;
};

module.exports = Asset;
