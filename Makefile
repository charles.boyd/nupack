.PHONY: all build test

MOCHA=./node_modules/mocha/bin/mocha
REPORTER=spec

all: build test

build:
	npm install

test:
	$(MOCHA) test/* --reporter=$(REPORTER)
