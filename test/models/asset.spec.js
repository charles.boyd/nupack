var _ = require('lodash');
var assert = require('assert');

var Asset = require('../../lib/models/Asset.js');

describe('Asset', function() {
  var asset = new Asset( 1000, 'foo' );

  it('#getValue()', function() {
    assert.equal( asset.getValue(), 1000 );
  });

  it('#getType()', function() {
    assert.equal( asset.getType(), 'foo' );
  });

});
