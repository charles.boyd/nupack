var _ = require('lodash');
var assert = require('assert');

var Job = require('../../lib/models/Job.js');
var Asset = require('../../lib/models/Asset.js');

var fooAsset = new Asset( 1000, 'foo' );
var barAsset = new Asset( 4000, 'bar' );

describe('Job', function() {

  var testJob = new Job();

  it('should have no assets initially', function() {
    assert.equal( testJob.netAssetValue(), 0.0 );
  });

  it('correctly appends assets to the job', function() {
    testJob.appendAsset(fooAsset);
    assert.equal( testJob.netAssetValue(), 1000 );
    testJob.appendAsset( barAsset );
    assert.equal( testJob.netAssetValue(), 5000 );
  });

  it('can increase and decrease the number of workers required for the job', function() {
    assert.equal( testJob.getWorkerCount(), 1 );
    testJob.setWorkerCount( 5 );
    assert.equal( testJob.getWorkerCount(), 5 );
    testJob.setWorkerCount( 10 );
    assert.equal( testJob.getWorkerCount(), 10 );
    testJob.setWorkerCount( 2 );
    assert.equal( testJob.getWorkerCount(), 2 );
  });

  it('requires worker count to be greater than zero', function() {
    var lastWorkerCount = testJob.getWorkerCount();
    testJob.setWorkerCount(-1);
    assert.equal( testJob.getWorkerCount(), lastWorkerCount );
    testJob.setWorkerCount(0);
    assert.equal( testJob.getWorkerCount(), lastWorkerCount );
  });

  it('requires the number of workers to be integral', function() {
    testJob.setWorkerCount(2.2);
    assert.equal( testJob.getWorkerCount(), 2 );
    testJob.setWorkerCount(3.6);
    assert.equal( testJob.getWorkerCount(), 4 );
  });

});
