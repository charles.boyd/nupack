var _ = require('lodash');
var assert = require('assert');
var nupack = require('../lib/nupack.js');

var testJob = nupack.getCurrentJob();
var markupTable = nupack.getMarkupTable();

var markupRates = {
  'base': {
    'flat': 0.05,
    'worker': 0.012
  },
  'asset': {
    'drugs': 0.075,
    'food': 0.13,
    'electronics': 0.02
  }
};

markupTable.setBaseMarkupRate( markupRates['base']['flat'] );
markupTable.setWorkerMarkupRate( markupRates['base']['worker'] );

_.forEach( _.keys(markupRates['asset']), function(assetType) {
  var rate = markupRates['asset'][assetType];
  markupTable.setMarkupRateForType(assetType, rate);
});

describe('NuPack', function() {

  afterEach(function() {
    nupack.resetJob();
  });

  it('conforms to example #1 in the description', function() {
    nupack.addAsset( 1299.99, 'food' );
    nupack.addWorkers(2);
    assert.equal( nupack.computeJobQuote(), 1591.58 );
  });

  it('conforms to example #2 in the description', function() {
    nupack.addAsset( 5432.00, 'drugs' );
    assert.equal( nupack.computeJobQuote(), 6199.81 );
  });

  it('conforms to example #3 in the description', function() {
    nupack.addAsset(12456.95, 'books');
    nupack.addWorkers(3);
    assert.equal( nupack.computeJobQuote(), 13707.63 );
  });

});
